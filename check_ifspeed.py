#!/bin/python

import os,sys
import argparse


#Create a proper "help" menu so the user understand what are the required parameters
parser = argparse.ArgumentParser(description='request interface input from user')
parser.add_argument('interface', metavar='interface', type=str, help='choose an interface name')
args = parser.parse_args()

#interface is the result of the user choice
interface = args.interface

#take the arguement of the user from above and insert it as a parameter in the command below, the command below will strip down the returned output of ethtool 'interface_name' and the result will be only the interface speed
filtertospeedonly = os.popen("ethtool "+ sys.argv[1]+" |grep -i speed | awk '{print $2}' |sed 's/Mb/ /g' | awk '{print $1}'").read()

filtertospeedonly = os.linesep.join([s for s in filtertospeedonly.splitlines() if s])

if not filtertospeedonly:
    print("UNKNOWN - Are you sure this is the correct interface name for this device?")
    sys.exit(3)

elif  int(filtertospeedonly) ==  10000:
        print("OK - your interface speed is set as desired - " + "ifSpeed is: " +  filtertospeedonly +'Mb/s')
        sys.exit(0)
elif int(filtertospeedonly) > 10000:
        print("OK - Your interface speed is set higher than 10G - " + "ifSpeed is:" + filtertospeedonly +'Mb/s')
        sys.exit(0)
elif int(filtertospeedonly) < 10000:
        print("CRITICAL - your interface speed is set lower than expected - " + "ifSpeed is:" + filtertospeedonly +'Mb/s')
        sys.exit(2)
