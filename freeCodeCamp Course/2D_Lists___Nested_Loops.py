Names = [
    ["Yoni", "Josh" , "Tom"],
    ["Yonatan", "Tomer", "Dan"],
    ["Logitech","Abyssus","Mac"],
    ["Jim", "Yossi", "Joshua"]
]
print(Names[1][2])


#Nested for loops :
for Name in Names:
    for object in Name:
        print(object)