#Practicing List function

#2 lists are mentioned below (lucky_numbers) (people)
lucky_numbers= [3, 29, 82, 13, 54, 41]
people= ["John ","Ben ", "Mike ", "Rami","Tomer","Tomer","Tomer"]
#_____________________________________________________________________________________________________________
#calling the people list and using the .extend function and then calling (lucky_numbers) list we have extended
#one list after the other,

#people.extend(lucky_numbers)
#print(people)
#_____________________________________________________________________________________________________________
#Using the append function we can append a new item onto the list
#people.append("Ben-al")
#print(people)
#______________________________________________________________________________________________________________
#lucky_numbers.insert(0, 43)
#print(lucky_numbers)
#_______________________________________________________________________________________________________________
#Removing elements from a list, we use the .remove function as follows
#people.remove("Rami")
#print(people)
#_______________________________________________________________________________________________________________
# Clearing a list entirely
#people.clear()
#print(people)
#_______________________________________________________________________________________________________________
# we can also use a cool function cool pop to "pop" the last item off of the list
#people.pop()
#print(people)
#_______________________________________________________________________________________________________________
#Printing an index of a string if exist
#print(people.index("Tomer"))
#_______________________________________________________________________________________________________________
#We can also count the amount of times an item appear on the list with the .count function
#print(people.count("Tomer"))
#_______________________________________________________________________________________________________________
#Sorting a list :
#people.sort()
#print(people)
#_______________________________________________________________________________________________________________
#We can also reverse the order of a list
#lucky_numbers.reverse()
#print(lucky_numbers)
#_______________________________________________________________________________________________________________
#We can also copy a list, for example :
#people2=people
#print(people2)
#_______________________________________________________________________________________________________________