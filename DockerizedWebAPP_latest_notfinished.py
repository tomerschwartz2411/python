import subprocess
import sys
import ansible_runner
import docker
from subprocess import check_output

# A function to determine if an installation of ansible is needed or not
def is_ansible_installed_or_not():
    # Created a variable to store the subprocess command
    isansibleinstalled = subprocess.Popen(["which", "ansible"], stdout=subprocess.PIPE)

    # "Catch" the standard output of the command 'which ansible'
    stdout_ofisansibleinstalled = isansibleinstalled.communicate()

    # Change the stdout to string
    rawpath = str(stdout_ofisansibleinstalled)

    # Remove uncessary stuff (that was created by subprocess) from the string.
    rawpath = rawpath[3:-10:]

    # if the word ansible exist in the stdout then let the user know where is ansible already installed,
    if 'ansible' in str(stdout_ofisansibleinstalled):
        print("ansible is already installed at " + rawpath)
        print('executing Ansible AD-HOC docker installation...')
        run_ansible_docker_adhoc()
    # else install ansible and only then continue
    else:
        USERANSWER = input("ansible isn't installed, would you like to install it? answer y/n only : ")
        if USERANSWER == 'y':
            print("Installing Ansible... ")
            aptget = subprocess.run(["sudo apt-get install ansible -y"], shell=True)
            print("executing Ansible AD-HOC docker installation...")
            run_ansible_docker_adhoc()

        if USERANSWER == 'n':
            print('exiting')
            sys.exit()

# function to run docker installation "playbookless" through ansible ad-hoc command
def run_ansible_docker_adhoc():
    # running the ansible runner, providing "working directory" , "host pattern" is the group i'm running this command on, module is the ansible module,
    # module args are the module arguments to pass to the module.
    adhoc_runner = ansible_runner.run(private_data_dir='/etc/ansible', host_pattern='localhost', module='apt',
                                      module_args='name=docker.io state=present')

    print("{}: {}".format(adhoc_runner.status, adhoc_runner.rc))
    for each_host_event in adhoc_runner.events:
        print(each_host_event['event'])
    print("Final status:")
    print(adhoc_runner.stats)
    ansible_runner.run


is_ansible_installed_or_not()


#def deploy_webapp():
#TBD



